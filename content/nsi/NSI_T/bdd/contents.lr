_model: page
---
title: Bases de données
---
date: 2020-06-23
---
image: bdd.jpg
---
toc: 1
---
body:

## Programme officiel
| Contenus | Capacités attendues |
|---|---|
| Modèle relationnel : relation, attribut, domaine, clef primaire, clef étrangère, schéma relationnel.| Identifier les concepts définissant le modèle relationnel.|
| Base de données relationnelle. | Savoir distinguer la structure d’une base de données de son contenu. <br/>Repérer des anomalies dans le schéma d’une base de données.|
| Système de gestion de bases de données relationnelles.| Identifier les services rendus par un système de gestion de bases de données relationnelles : persistance des données, gestion des accès concurrents, efficacité de traitement des requêtes, sécurisation des accès.|
| Langage SQL : requêtes d’interrogation et de mise à jour d’une base de données. | Identifier les composants d’une requête. <br/>Construire des requêtes d’interrogation à l’aide des clauses du langage SQL : SELECT, FROM, WHERE, JOIN. <br/>Construire des requêtes d’insertion et de mise à jour à l’aide de : UPDATE, INSERT, DELETE. |

## Introduction du thème

Cette vidéo de Class'Cod va introduire la problématique des bases de données que nous allons étudier dans cette séquence.

<iframe width="560" height="315" src="https://www.youtube.com/embed/iu8z5QtDQhY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Séquences

0. [Introduction aux bases de données](intro/)
0. [Le modèle relationnel](modrel/)

## TP Jupyter
0. [Découverte du langage SQL](https://notebooks.lecluse.fr/nsi/terminale/bases%20de%20donn%C3%A9es/sql/tp/2020/06/23/nsi_t_SQL.html)
0. [Interroger efficacement une BDD](https://notebooks.lecluse.fr/nsi/terminale/bases%20de%20donn%C3%A9es/sql/tp/2020/06/23/nsi_t_SQL2.html)
0. [Manipuler les données d'une BDD](https://notebooks.lecluse.fr/nsi/terminale/bases%20de%20donn%C3%A9es/sql/tp/2020/06/23/nsi_t_SQL3.html)
0. [Bases de données avec Python](https://notebooks.lecluse.fr/python/nsi/terminale/bases%20de%20donn%C3%A9es/sql/tp/2020/06/25/nsi_t_PythonSql.html)
0. [Exploiter un vrai jeu de donnée](https://notebooks.lecluse.fr/python/nsi/terminale/bases%20de%20donn%C3%A9es/sql/tp/2020/06/25/nsi_t_baseDataGouv.html)

Si vous avez été au bout du TP, vous devriez comprendre cette image 

![bobby tables](https://imgs.xkcd.com/comics/exploits_of_a_mom.png )

## sources
- Le site <a href="https://pixees.fr/informatiquelycee/" target="_new">pixees</a> de David Roche
- Le <a href="https://mermet.users.greyc.fr/Enseignement/EnseignementInformatiqueLycee/Havre/Bloc4/index.html" target="_new">cours DIU-IL</a> de Bruno Mermet 