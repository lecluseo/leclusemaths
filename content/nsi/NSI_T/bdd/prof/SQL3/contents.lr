_model: page
---
toc: 2
---
title: TP SQL 3/3
---
body:
# Manipuler les données avec SQL

Dans ce dernier TP, nous allons voir comment insérer, mettre à jour ou supprimer des enregistrements dans des tables.

Commencez par valider les cellules suivantes afin d'activer la fonctionnalité SQL de Jupyter et de charger la base du TP précédent.


```python
%load_ext sql

%sql sqlite:///livres_db
```




    'Connected: @livres_db'



## Insérer un enregistrement dans une table

Nous avons déjà rencontré cerre requète **INSERT**. Elle s'applique que la table soit vide ou nom. On peut très bien ajouter par exemple une troisième langue dans notre table **Langues**


```python
%sql INSERT INTO Langues (Langue) VALUES('Klingon');
```

     * sqlite:///livres_db
    1 rows affected.





    []




```python
# Vérifions :
%sql SELECT * FROM Langues;
```

     * sqlite:///livres_db
    Done.





<table>
    <tr>
        <th>IdLangue</th>
        <th>Langue</th>
    </tr>
    <tr>
        <td>1</td>
        <td>Anglais</td>
    </tr>
    <tr>
        <td>2</td>
        <td>Français</td>
    </tr>
    <tr>
        <td>3</td>
        <td>Klingon</td>
    </tr>
</table>



## Mise a jour, effacement : UPDATE et DELETE

Les requêtes **UPDATE** et **DELETE** fonctionnent sur le même modèle que les requêtes **SELECT**.  Attention, on a vite fait d'effacer toutes ses données si on ne configure pas bien sa requête. Une bonne habitude à prendre est de tester d'abord ses critères à l'aide d'un **SELECT**.

Observez les exemples ci-dessous :


```python
%sql SELECT NomAuteur FROM Auteurs WHERE IdAuteur = 10;
```

     * sqlite:///livres_db
    Done.





<table>
    <tr>
        <th>NomAuteur</th>
    </tr>
    <tr>
        <td>Verne</td>
    </tr>
</table>



C'est bien lui !!

Modifions le nom de l'auteur grâce à une requête de mise à jour : 

**UPDATE** *table* <br>
**SET**  *attribut1* = *valeur1*, *attribut2* = *valeur2*, ...<br>
**WHERE** *critère*;


```python
%%sql 

UPDATE Auteurs 
SET NomAuteur = "Ze Djloule", PrenomAuteur = "Juju"
WHERE IdAuteur=10;

SELECT * FROM Auteurs ;
```

     * sqlite:///livres_db
    1 rows affected.
    Done.





<table>
    <tr>
        <th>IdAuteur</th>
        <th>NomAuteur</th>
        <th>PrenomAuteur</th>
        <th>IdLangue</th>
        <th>AnneeNaissance</th>
    </tr>
    <tr>
        <td>1</td>
        <td>Orwell</td>
        <td>George</td>
        <td>1</td>
        <td>1903</td>
    </tr>
    <tr>
        <td>2</td>
        <td>Herbert</td>
        <td>Frank</td>
        <td>1</td>
        <td>1920</td>
    </tr>
    <tr>
        <td>3</td>
        <td>Asimov</td>
        <td>Isaac</td>
        <td>1</td>
        <td>1920</td>
    </tr>
    <tr>
        <td>4</td>
        <td>Huxley</td>
        <td>Aldous</td>
        <td>1</td>
        <td>1894</td>
    </tr>
    <tr>
        <td>5</td>
        <td>Bradbury</td>
        <td>Ray</td>
        <td>1</td>
        <td>1920</td>
    </tr>
    <tr>
        <td>6</td>
        <td>K. Dick</td>
        <td>Philip</td>
        <td>1</td>
        <td>1928</td>
    </tr>
    <tr>
        <td>7</td>
        <td>Barjavel</td>
        <td>René</td>
        <td>2</td>
        <td>1911</td>
    </tr>
    <tr>
        <td>8</td>
        <td>Boulle</td>
        <td>Pierre</td>
        <td>2</td>
        <td>1912</td>
    </tr>
    <tr>
        <td>9</td>
        <td>Van Vogt</td>
        <td>Alfred Elton</td>
        <td>1</td>
        <td>1912</td>
    </tr>
    <tr>
        <td>10</td>
        <td>Ze Djloule</td>
        <td>Juju</td>
        <td>2</td>
        <td>1828</td>
    </tr>
</table>



supprimons vite cette entrée ! Nous utiliseerons une requête
**DELETE FROM** *table* **WHERE** *critere*

**Attention** : soyez bien sûr de votre critère sous peine de perdre des données importantes !


```python
%%sql 

DELETE  FROM Auteurs  WHERE IdAuteur=10;

SELECT * FROM Auteurs;
```

     * sqlite:///livres_db
    1 rows affected.
    Done.





<table>
    <tr>
        <th>IdAuteur</th>
        <th>NomAuteur</th>
        <th>PrenomAuteur</th>
        <th>IdLangue</th>
        <th>AnneeNaissance</th>
    </tr>
    <tr>
        <td>1</td>
        <td>Orwell</td>
        <td>George</td>
        <td>1</td>
        <td>1903</td>
    </tr>
    <tr>
        <td>2</td>
        <td>Herbert</td>
        <td>Frank</td>
        <td>1</td>
        <td>1920</td>
    </tr>
    <tr>
        <td>3</td>
        <td>Asimov</td>
        <td>Isaac</td>
        <td>1</td>
        <td>1920</td>
    </tr>
    <tr>
        <td>4</td>
        <td>Huxley</td>
        <td>Aldous</td>
        <td>1</td>
        <td>1894</td>
    </tr>
    <tr>
        <td>5</td>
        <td>Bradbury</td>
        <td>Ray</td>
        <td>1</td>
        <td>1920</td>
    </tr>
    <tr>
        <td>6</td>
        <td>K. Dick</td>
        <td>Philip</td>
        <td>1</td>
        <td>1928</td>
    </tr>
    <tr>
        <td>7</td>
        <td>Barjavel</td>
        <td>René</td>
        <td>2</td>
        <td>1911</td>
    </tr>
    <tr>
        <td>8</td>
        <td>Boulle</td>
        <td>Pierre</td>
        <td>2</td>
        <td>1912</td>
    </tr>
    <tr>
        <td>9</td>
        <td>Van Vogt</td>
        <td>Alfred Elton</td>
        <td>1</td>
        <td>1912</td>
    </tr>
</table>



### Attention à la cohérence des données

Dans une base de données relationnelle il faut être vigilant lors de la suppression d'enregistrements : en effet la suppression d'un enregistrement entraîne la suppression de sa clé primaire qui peut être utilisée en tant que clé externe dans une autre table. Cela entraîne la corruption des données. Observez l'exemple ci-dessous dans lequel je me suis attribué un livre célèbre sans toucher à la table **Livres**

Il existe en SQL des moyens pour se prémunir de ce type de problèmes mais cela dépasse le cadre de ce cours.


```python
%%sql
INSERT INTO Auteurs 
    (NomAuteur, PrenomAuteur, IdLangue, AnneeNaissance) 
VALUES
    ("Lecluse", "Olivier", 2, 1850);
    
SELECT Titre, NomAuteur from Livres JOIN Auteurs USING (IdAuteur);
```

     * sqlite:///livres_db
    1 rows affected.
    Done.





<table>
    <tr>
        <th>Titre</th>
        <th>NomAuteur</th>
    </tr>
    <tr>
        <td>1984</td>
        <td>Orwell</td>
    </tr>
    <tr>
        <td>Dune</td>
        <td>Herbert</td>
    </tr>
    <tr>
        <td>Fondation</td>
        <td>Asimov</td>
    </tr>
    <tr>
        <td>Le meilleur des mondes</td>
        <td>Huxley</td>
    </tr>
    <tr>
        <td>Fahrenheit 451</td>
        <td>Bradbury</td>
    </tr>
    <tr>
        <td>Ubik</td>
        <td>K. Dick</td>
    </tr>
    <tr>
        <td>Chroniques martiennes</td>
        <td>Bradbury</td>
    </tr>
    <tr>
        <td>La nuit des temps</td>
        <td>Barjavel</td>
    </tr>
    <tr>
        <td>Blade Runner</td>
        <td>K. Dick</td>
    </tr>
    <tr>
        <td>Les Robots</td>
        <td>Asimov</td>
    </tr>
    <tr>
        <td>La Planète des singes</td>
        <td>Boulle</td>
    </tr>
    <tr>
        <td>Ravage</td>
        <td>Barjavel</td>
    </tr>
    <tr>
        <td>Le Maître du Haut Château</td>
        <td>K. Dick</td>
    </tr>
    <tr>
        <td>Le monde des A</td>
        <td>Van Vogt</td>
    </tr>
    <tr>
        <td>La Fin de l’éternité</td>
        <td>Asimov</td>
    </tr>
    <tr>
        <td>De la Terre à la Lune</td>
        <td>Lecluse</td>
    </tr>
</table>



### A vous de jouer

1. Réinsérez l'auteur Jules Verne à sa place !
1. Supprimez tous les livres écrits au 19e sciecle


```python
%%sql

/* 
### BEGIN SOLUTION 
*/

UPDATE Auteurs 
SET NomAuteur = "Verne", PrenomAuteur = "Jules", AnneeNaissance = 1828
WHERE NomAuteur = "Lecluse";

DELETE FROM Livres WHERE AnneePubli < 1900;

/* 
### END SOLUTION 
*/
```

     * sqlite:///livres_db
    Done.
    1 rows affected.
    0 rows affected.





    []




```python
result = %sql SELECT Titre, NomAuteur AnneePubli from Livres JOIN Auteurs USING (IdAuteur);
```

     * sqlite:///livres_db
    Done.



```python
assert len(result) == 15
### BEGIN HIDDEN TESTS
result[-1] == ('La Fin de l’éternité', 'Asimov')
### END HIDDEN TESTS
```




    True




```python

```
