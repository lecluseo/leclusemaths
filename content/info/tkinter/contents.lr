_model: page
---
title: TKinter
---
image: pythont.png
---
body:

## Initiation à TKInter

[TKinter](https://docs.python.org/fr/3/library/tkinter.html) est une librairie Python permettant de développer des interfaces graphiques. L'approche proposée est assez basique et permet de développer des applications simples mettant en œuvre des interfaces graphiques.

Pour développer des projets avec *TKinter*, il est recommandé d'utiliser un véritable environnement de développement. Python est en général installé avec un environnement basique nommé *IDLE*. Pour des fonctionnalités plus avancées, il est possible de regarder du côté de 
- <a href="https://www.jetbrains.com/fr-fr/pycharm/" target="_new">pycharm</a> en version *community edition* gratuite
- <a href="https://vscodium.com/" target="_new">VScodium</a> dérivé de l'éditeur Microsoft VSCode

## Tutoriels

0. [Hello World](helloworld/)
0. [Texte variable](textevariable/)
0. [Boutons radio](boutonsradio/)
0. [Ligne de saisie](lignesaisie/)
0. [Les surfaces graphiques (canvas)](canvas/)
0. [Les événements souris](mouseevent/)
0. [Les événements clavier](kbdevent/)
0. [Jeu de carte](jeucarte/)
0. [Animation](animation/)
0. [Les dialogues](dialogues/)
0. [Les menus](menus/)


## Quelques liens utiles
- <a href="http://fsincere.free.fr/isn/python/cours_python_tkinter.php" target="_new">le site de Fabrice Sincère</a> duquel un certain nombre d'idées de ce tutoriel sont tirées
- <a href="http://tkinter.fdex.eu/" target="_new">la doc de référence TKinter</a> traduite en français
---
date: 2020-02-23
---
description: Initiation à TKinter
