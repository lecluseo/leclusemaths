_model: page
---
title: Compteur de personnes
---
date: 2020-02-26
---
toc: 2
---
body:

durée : 2 à 3h

L'objectif de ce projet est de réaliser un compteur de personne à l'entrée d'une salle de spectacle. Cette salle contient au maximum 120 personnes. 
Pour des raisons de sécurité, on doit s'assurer de ne pas laisser entrer plus de personnes que le nombre autorisé.

Nous allons donc réaliser un compteur de personnes à l'aide de la carte micro:bit. Ce projet en apparence banale est en réalité bien plus riche qu'il n'y paraît comme vous allez le constater !

## Mise en oeuvre matérielle

Dans ce projet, nous utiliserons 
- un capteur : les boutons de la carte micro:bit
- un actionneur : l'écran de 25 LED

## Première version

### diagramme fonctionnel

Voici la manière dont le projet va être implémenté

![diagramme](diagramme1.png)

L'algorithme est donc le suivant :
```
nb_spectateurs <- 0
Répéter indéfiniement:
    Si bouton_A a été préssé :
        nb_spectateurs <- nb_spectateurs + 1
        afficher nb_spectateurs
    fin Si
Fin Répéter
```

Sa traduction en langage Python est imédiate :
```python
import microbit as mb

nb_spectateurs = 0

# Répéter indéfiniement
while True:
    if mb.button_a.was_pressed():
        nb_spectateurs = nb_spectateurs + 1
        mb.display.show(nb_spectateurs)
```

### Au travail !

0. Flashez ce programme sur la carte et vérifiez son bon fonctionement.
0. A présent, modifiez ce programme afin qu'un appui sur le ***bouton B** remette le compteur à 0.
0. Pour terminer modifiez le programme de manière à ce qu'il détecte lorsque le nombre maximum de 120 personnes est atteint. Il affichera alors à l'écran un smiley triste.

## Seconde version connectée !

Nous allons maintenant entrer dans l'univers des objets connectés. En effet, notre salle de spectacle possède plusieurs entrées. Il faut donc que les personnes enregistrées à chaque entrée soient totalisées sur un dispositif unique afin de vérifier que la jauge totale n'est pas dépassée.

Pour cela, nous allons envisager
- Plusieurs cartes **émetteur** pour compter les personnes : une pour chaque entrée.
- Une carte **maître** centralise le nombre total d'entrées.
- Les cartes vont communiquer entre-elles via liaison radio. Elles seront donc *connectées*.

### Définir un protocole de communication

Nous devons imaginer des règles communes de communication entre la ou les cartes **émettrices** et la carte **maître**. 
Commençons par quelque chose de très simple : Le module radio permet d'envoiyer un texte (*chaine de caractères*) d'une carte à l'autre.
Décidons donc d'un protocole très basique : l'envoi du texte "A" par une carte **émettrice** provoque sur la carte **maitre** l'incrémentation du nombre de spectateur.
On peut le schématiser ainsi :

![protocole](protocole1.png)

Quels avantages et limitations voyez-vous à l'utilisation de ce protocole ?

### Algorithmes

Nous allons commencer par décrire les algorithmes sur les deux cartes. Ceux-ci sont très simples puisque nous avons choisi un protocole de communication rudimentaire.

#### sur la carte **émettrice**

```
Configurer la radio
Répéter indéfiniement:
    Si le bouton A a été préssé:
        Envoyer le message "A"
    fin Si
```

#### sur la carte **maître**
```
Configurer la radio
nb_spectateurs <- 0
Répéter indéfiniement :
    Si on reçoit le message "A":
        nb_spectateurs <- nb_spectateurs + 1
    fin Si
    Si le bouton A a été préssé:
        Afficher nb_spectateurs sur l'écran LED
    fin Si
```
### Programmes en Python

Voici les programmes python correspondants : C'est la traduction basique des algorithmes. 
Pour la saisie des programmes vous devrez procéder par groupes de 4 :
deux élèves prendront en charge la carte **émettrice**, deux élèves prendront en charge la carte **maître**.
Nous utiliserons donc 10 cartes pour 20 élèves.

#### sur la carte **émettrice**

Tapez le programme python suivant :

```python
import microbit as mb
import radio

# Configuration radio
radio.config(group=1)
radio.on()

while True:
    if mb.button_a.was_pressed():
        radio.send("A")
```

#### sur la carte **maître**

Tapez le programme python suivant :

```python
import microbit as mb
import radio

# Configuration radio
radio.config(group=1)
radio.on()

nb_spectateurs = 0
while True:
    if radio.receive() == "A":
        nb_spectateurs = nb_spectateurs+1
    if mb.button_a.was_pressed():
        mb.display.show(nb_spectateurs)
```

#### Explications

Les programmes sont assez simples et vous possédez les bases pour les comprendre dans l'ensemble. 

Nous allons toutefois décrire le fonctionnement du module radio : 
- Il faut commencer par importer le module **radio**, celui-ci ne faisant pas partie de Python standard.
- On configure celui-ci en paramétrant un groupe (ici 1). Toutes les cartes dans un même groupes peuvent communiquer. Si chaque groupe d'élèves choisissent le groupe 1, les compteurs intéragiront d'un groupe à l'autre. Changez le numéro du groupe pour vous isoler des autres élèves. Les numéros de groupe vont de 0 à 255.
- La radio consomme de l'énergie, elle est donc éteinte par défaut. Pour l'utiliser, il faut donc invoquer la méthode `radio.on()`.
- Pour savoir si un message a été reçu, on appelle `radio.receive()`. Cet appel renverra
    - "" (une chaîne vide) si aucun message n'a été reçu. Une chaîne vide est évaluée à `False` dans la structure conditionnelle **If**
    - une chaîne contenant un message (ici "A") si un émetteur à transmis un message. Une chaîne non vide est évaluée à `True` dans la structure conditionnelle **If**
- Pour émettre un message, on invoque simplement `radio.send(message)` où la chaîne *message* contient le texte à envoyer.

### A vous de jouer

0. Par groupes de 4 ($2\times 2$), programmez une carte **émettrice** et une carte **maître**
0. Tester le fonctionnement du dispositif. Remarquez que vos cartes **maîtres** sont actionnées par les cartes **émettrices** des autres groupes
0. Imaginer une extension du protocole de communication afin que la carte **maître** puisse signaler aux cartes des personnes aux entrées de la salle lorsque la jauge de 120 personnes a été atteinte.
0. Modifiez les programmes de manière à que que les cartes des personnes aux entrées affichent un smiley triste lorsque 120 personnes sont entrées.

### Pour les plus aguerris

On peut repenser l'**IHM** pour afficher d'un bloc un nombre de spectateurs entre 0 et 120 :
Le compteur s’affiche sur la matrice LED sous forme de points : 
- les 4 premières lignes permettent de compter jusqu’à 20
- la dernière ligne compte le nombre de pages de 20 complétées. 
On a ainsi un affichage condensé jusqu’à 120 sur un seul écran !

Quel est le nombre de personnes correspondant à l'écran ci-dessous ?

![ihm120](ihm120.png)

Réalisez la modification dans le programme **maître** de manière à ce que l'écran s'actualise en temps réel au fur à mesure de la réception d'une nouvelle entrée, sans avoir besoin d'appuyer sur le **bouton A**

On pourra pour cela utiliser la fonction `mb.display.set_pixel(x, y, couleur)` où
- `x` et `y` désignent les coordonnées de la LED à allumer (respectivement **N° de colonne** et **N° de ligne**)
-  `couleur` est un entier de 0 à 9 définissant l'illumination de chaque LED (0 = éteint, 9 = allumé au maximum)
